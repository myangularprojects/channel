import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-grid-component',
  templateUrl: './grid-component.component.html',
  styleUrls: ['./grid-component.component.css']
})
export class GridComponentComponent {
  @Input() gridColumnList:any;
  @Input() gridDataList:any;
  @Input() gridConfig:any;

}
