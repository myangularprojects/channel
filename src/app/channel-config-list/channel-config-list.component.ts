import { Component, Input } from '@angular/core';
import { ColDef } from 'ag-grid-community';

@Component({
  selector: 'app-channel-config-list',
  templateUrl: './channel-config-list.component.html',
  styleUrls: ['./channel-config-list.component.css']
})
export class ChannelConfigListComponent {
  @Input() public channelList:any;

  public columnDefs: ColDef[] = [
    {field:"channelName"},
    {field:"channelType"},
    {field:"userName"},
    {field:"password"}
  ];
  
  public defaultColDef: ColDef = {
    sortable: true,
    filter: true,
  };

}
