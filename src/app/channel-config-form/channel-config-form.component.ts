import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-channel-config-form',
  templateUrl: './channel-config-form.component.html',
  styleUrls: ['./channel-config-form.component.css']
})
export class ChannelConfigFormComponent{
public channels:any;

public channelAndType:any={
    SMS:[
      {
        type:"AWS",
      },
      {
        type:"AWS one",
      },
      {
        type:"AWS two",
      }
    ],
    EMAIL:[
      {
        type:"MONTRIL"
      },
      {
        type:"MONTRIL one",
      },
      {
        type:"MONTRIL two",
      }
    ],
    WHATSAPP:[
      {
        type:"Encrypted"
      },
      {
        type:"Encrypted one",
      },
      {
        type:"Encrypted two",
      }
    ]
  };
  constructor(){ 
  this.channels=Object.keys(this.channelAndType)
  }

  public channel:any=[];
channelSelect(event:any){
  
    this.channel=this.channelAndType[event.target.value];
    console.log(this.channel);

}

channelForm:any=new FormGroup({
channelName:new FormControl(''),
channelType:new FormControl(''),
userName:new FormControl(''),
password:new FormControl('')
});

public formValue:any=[];

public addValue(){
this.formValue.push(this.channelForm.value);
this.formValue=[...this.formValue];
}

}
